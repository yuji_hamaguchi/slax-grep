(ns slax-grep.core
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [compojure.handler :as handler]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [clj-http.client :as client]
            [ring.middleware.json :as json]
            [ring.util.response :refer [response]]
            [clojure.data.json :as data-json]
            [clj-time.core :as time-core]
            [clj-time.local :as local-time]
            [clj-time.coerce :as time-coerce]
            [clj-time.format :as time-format]))

;実行例
;APIの実行
;curl -XGET "http://localhost:3001/slax/grep?user_name=foo&token=4BxgxxiihzrSU3KrSBirlTEG&team_domain=uzabase&channel_name=slax&text=grep%20foo%20%40yuji.hamaguchi" | native2ascii -reverse -encoding UTF-8
;JSONに対する処理
;curl -XPOST -H "Content-Type: application/json" "http://localhost:3001/slax/grep?user_name=foo&token=4BxgxxiihzrSU3KrSBirlTEG&team_domain=uzabase&channel_name=slax&text=grep%20baz" -d @tmp.json | native2ascii -reverse -encoding UTF-8
;tmp.json
;[{"ts":"1509928291.000018",
;      "channel-name":"slax",
;  "username":"yuji.hamaguchi",
;  "text":"foobarbaz",
;  "permalink":"https:\/\/uzabase.slack.com\/archives\/C7SCETXCL\/p1509928291000018"}]


(def token "xoxp-2466069044-8547250964-262986284112-aa6312157b99ed352fe4730d443beb5f")

(defn execute-slack-api
  [search-str target]
  (println "called with search-str:" search-str "and target:" target ".")
  (let [result (-> (client/get "https://slack.com/api/search.messages"
                               {:query-params {"token"  token
                                               "query"  (str search-str " " (case (first target)
                                                                              \@ (str "from:" target)
                                                                              \# (str "in:" target)
                                                                              :else nil))
                                               "sort"   "timestamp"
                                               "pretty" 1}}
                               {:accept :json})
                   :body
                   (data-json/read-str :key-fn keyword)
                   :messages
                   :matches)
        result' (map (fn [m] (assoc m :channel-name (:name (:channel m)))) result)]
    (data-json/write-str (map #(select-keys % [:ts :channel-name :username :text :permalink]) result'))))

(defn sx-grep
  [search-str ms]
  (println "called with search-str:" search-str "and" (count ms) "messages.")
  (data-json/write-str
    (filter
      (fn [obj] (re-find (re-pattern search-str) (:text obj))) ms)))

(defn command-handler
  [text & sx-univ-objs]
  (let [[command search-str target] (clojure.string/split text #"\s")
        target (if target (clojure.string/replace target #"[<>]" ""))]
    (if-not sx-univ-objs
      (execute-slack-api search-str target)
      (let [sx-univ-objs' (map
                            (fn [obj] (reduce (fn [acc [k v]] (assoc acc (keyword k) v)) {} obj))
                            (first sx-univ-objs))]
        (sx-grep search-str sx-univ-objs')))))

(defn slax-gateway
  [token team_domain channel_name text user_name & sx-univ-objs]
  (when (not (= user_name "slackbot"))
    (if (and (= token "4BxgxxiihzrSU3KrSBirlTEG")
             (= team_domain "uzabase")
             (= channel_name "slax"))
      (if-not sx-univ-objs
        (response (->> (command-handler text)))
        (response (->> (command-handler text (first sx-univ-objs)))))
      (response {:text "Authentication Failed"}))))

(defroutes app-routes
           (GET "/slax/grep" [token team_domain channel_name text user_name]
                (slax-gateway token team_domain channel_name text user_name))
           (POST "/slax/grep" {{token        :token
                                team_domain  :team_domain
                                channel_name :channel_name
                                text         :text
                                user_name    :user_name} :params
                               sx-univ-objs              :body}
                 (slax-gateway token team_domain channel_name text user_name sx-univ-objs))
           (route/not-found "Not Found"))

(def app
  (-> (handler/api app-routes)
      (json/wrap-json-body)
      (json/wrap-json-response)))
